(function () {
    "use strict";

    var fs = require('fs');

    module.exports = function (grunt) {

        var appJsFiles = [
            'app/app.js',
            'app/plugins/plugins.js',
            'app/areas/**/*.js',
            'app/listeners/*.js',
            'app/services/**/*.js',
            'app/directives/**/*.js',
            'app/translations/**/*.js'
        ];

        var allJsFiles = function() {
            return [
                'Gruntfile.js',
                'karma.conf.js',
                'e2e-tests/**/*.js',
                'unit-tests/**/*.js'
            ].concat(appJsFiles);
        };

        var lessFiles = [
            'app/style/**/*.less'
        ];

        var libFiles = [
            'app/bower_components/jquery/dist/jquery.js',
            'app/bower_components/moment/min/moment-with-locales.js',
            'app/bower_components/angular/angular.js',
            'app/bower_components/angular-utf8-base64/angular-utf8-base64.min.js',
            'app/bower_components/angular-loader/angular-loader.js',
            'app/bower_components/angular-mocks/angular-mocks.js',
            'app/bower_components/angular-moment/angular-moment.js',
            'app/bower_components/angular-resource/angular-resource.js',
            'app/bower_components/angular-route/angular-route.js',
            'app/bower_components/angular-sanitize/angular-sanitize.js',
            'app/bower_components/angular-translate/angular-translate.js',
            'app/bower_components/pdfjs-dist/build/pdf.js',
            'app/bower_components/angular-pdf-viewer/dist/angular-pdf-viewer.min.js',
            'app/bower_components/ng-idle/angular-idle.min.js',
            'app/bower_components/opentele-client-commons/dist/deviceListeners.js',
            'app/bower_components/opentele-client-commons/dist/questionnaireParser.js',
            'app/bower_components/opentele-client-commons/dist/native.js',

            'app/bower_components/jqplot/jquery.jqplot.js',
            'app/bower_components/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js',
            'app/bower_components/jqplot/plugins/jqplot.canvasAxisTickRenderer.js',
            'app/bower_components/jqplot/plugins/jqplot.canvasTextRenderer.js',
            'app/bower_components/jqplot/plugins/jqplot.canvasOverlay.js',
            'app/bower_components/jqplot/plugins/jqplot.dateAxisRenderer.js',
            'app/bower_components/jqplot/plugins/jqplot.cursor.js',
            'app/bower_components/jqplot/plugins/jqplot.highlighter.js'
        ];

        var libFilesMin = libFiles.map(function(filename) {
            var candidate = filename.slice(0,-3).concat('.min.js');
            return fs.existsSync(candidate) ? candidate : filename;
        });

        var htmlFiles = [
            'app/*.html',
            'app/directives/**/*.html',
            'app/areas/**/*.html',
            'app/listeners/measurementTemplates/*.html',
            'app/services/parserServices/nodeTemplates/**/*.html'
        ];

        var properties = grunt.file.readJSON('package.json');

        // Example: grunt dev --env=dev --serverUrl=http://192.168.1.11:8090/opentele-citizen-server
        var env = grunt.option('env') || 'dev';
        var url = grunt.option('serverUrl');
        var packageName = grunt.option('packageName') || properties.name;
        var config = {
            serverUrl: url,
            environment: env,
            packageName: packageName
        };

        // Example: grunt dev --plugins=fakeNativeLayer
        var pluginsOption = grunt.option('plugins') || null;
        var plugins = pluginsOption !== null ?
            pluginsOption.replace(" ", "").split(',') :
            [];
        plugins.forEach(function(plugin) {
            var pluginPath = 'app/plugins/' + plugin + '/';
            appJsFiles.push(pluginPath + '**/*.js');
            htmlFiles.push(pluginPath + '**/*.html');
            lessFiles.push(pluginPath + '**/*.less');
        });

        // Example: grunt ci-e2etest-chrome --scenarios=delayNode
        var e2ePrefix = 'e2e-tests/areas/';
        var protractorSpecs = [e2ePrefix + '**/*.js'];
        var scenarios = grunt.option('scenarios') || null;
        if (scenarios !== null) {
            var scenariosPath = e2ePrefix + '**/' +
                scenarios + 'Scenarios.js';
            protractorSpecs = ['e2e-tests/areas/**/*Page.js']
                .concat(scenariosPath);
        }

        grunt.initConfig({
            pkg: properties,
            cfg: config,
            // Task configurations
            bower: {
                install: {
                    options: {
                        install: true,
                        copy: false,
                        targetDir: 'app/bower_components',
                        cleanTargetDir: false
                    }
                }
            },

            "file-creator": {
                "plugins": {
                    "app/plugins/plugins.js": function (fs, fd, done) {
                        var enabledPlugins = plugins.map(function(plugin) {
                            return 'opentele.' + plugin;
                        });
                        fs.writeSync(fd, "(function() {" + '\n');
                        fs.writeSync(fd, "    'use strict';" + '\n');
                        fs.writeSync(fd, '\n');
                        fs.writeSync(fd, "    var plugins = angular.module('opentele.plugins', " +
                                     JSON.stringify(enabledPlugins) + ");" + '\n');
                        fs.writeSync(fd, "}());" + '\n');
                        done();
                    },
                    "app/plugins/plugins.less": function(fs, fd, done) {
                        plugins.forEach(function(plugin) {
                            if (fs.existsSync("app/plugins/" + plugin + "/" + plugin + ".less")) {
                                var stmt = '@import "' + plugin + '/' + plugin + '";';
                                fs.writeSync(fd, stmt);
                            }
                        });
                        done();
                    }
                }
            },

            jshint: {
                all: allJsFiles()
            },

            complexity: {
                generic: {
                    src: appJsFiles,
                    options: {
                        breakOnErrors: false,
                        jsLintXML: 'test_out/complexity-report.xml',
                        checkstyleXML: 'test_out/checkstyle.xml',
                        errorsOnly: false,
                        cyclomatic: [3,7,12],
                        halstead: [8,13,20],
                        maintainability: 100,
                        hideComplexFunctions: false,
                        broadcast: false
                    }
                }
            },

            karma: {
                options: {
                    configFile: 'karma.conf.js'
                },
                debug: {
                    browsers: ['Chrome'],
                    autoWatch: false,
                    singleRun: false
                },
                unit: {
                    browsers: ['Chrome'],
                    singleRun: true
                },
                ciUnit: {
                    browsers: ['PhantomJS'],
                    singleRun: true,
                    preprocessors: {
                        'app/app.js': ['coverage'],
                        'app/areas/**/*.js': ['coverage'],
                        'app/directives/**/*.js': [],
                        'app/listeners/**/*.js': ['coverage'],
                        'app/services/**/*.js': ['coverage'],
                        'app/translations/**/*.js': ['coverage']
                    },
                    reporters: ['progress', 'junit', 'coverage', 'threshold']
                },
                continous: {
                    singleRun: false,
                    autoWatch: true
                }
            },

            html2js: {
                options: {
                    base: 'app'
                },
                dist: {
                    src: htmlFiles,
                    dest: 'app/tmp/templates.js'
                }
            },

            concat: {
                options: {
                    separator: '\n'
                },
                dev: {
                    files: {
                        'app/dist/app.js': appJsFiles.concat(['app/tmp/*.js']),
                        'app/dist/libs.js': libFiles
                    }
                },
                prod: {
                    files: {
                        'app/dist/app.js': appJsFiles.concat(['app/tmp/*.js']),
                        'app/dist/libs.js': libFilesMin
                    }
                }
            },

            less: {
                dev: {
                    files: {
                        "app/style.css": "app/style/style.less"
                    }
                },
                prod: {
                    options: {
                        cleancss: true
                    },
                    files: {
                        "app/style.css": "app/style/style.less"
                    }
                }
            },

            lesslint: {
                src: ['app/style/**/*.less']
            },

            ngAnnotate: {
                options: {
                    add: true
                },
                opentele: {
                    files: {
                        'app/dist/app.js': ['app/dist/app.js']
                    }
                }
            },

            uglify: {
                dist: {
                    files: {
                        'app/dist/app.js': ['app/dist/app.js']
                    },
                    options: {
                        mangle: false
                    }
                }
            },

            clean: {
                temp: {
                    src: ['app/tmp']
                },
                dist: {
                    src: ['dist/']
                }
            },

            watch: {
                dev: {
                    files: allJsFiles().concat(htmlFiles).concat(lessFiles),
                    tasks: ['jshint', 'less:dev', 'html2js:dist', 'karma:unit',
                        'concat:dev', 'ngAnnotate:opentele', 'clean:temp'],
                    options: {
                        atBegin: true
                    }
                },
                devCompile: {
                    files: allJsFiles().concat(htmlFiles).concat(lessFiles),
                    tasks: ['jshint', 'less:dev', 'html2js:dist', 'concat:dev',
                        'ngAnnotate:opentele', 'clean:temp'],
                    options: {
                        atBegin: true
                    }
                },
                devDocs: {
                    files: allJsFiles().concat(htmlFiles).concat(lessFiles),
                    tasks: ['jshint', 'less:dev', 'html2js:dist', 'ngdocs',
                        'concat:dev', 'ngAnnotate:opentele', 'clean:temp'],
                    options: {
                        atBegin: true
                    }
                },
                min: {
                    files: allJsFiles().concat(htmlFiles).concat(lessFiles),
                    tasks: ['jshint', 'less:prod', 'html2js:dist', 'karma:unit',
                        'concat:dev', 'ngAnnotate:opentele', 'clean:temp',
                        'uglify:dist'],
                    options: {
                        atBegin: true
                    }
                }
            },

            connect: {
                app: {
                    options: {
                        hostname: '*',
                        port: 8000
                    }
                },
                package: {
                    options: {
                        port: 8000,
                        hostname: 'localhost',
                        base: 'dist/package/'
                    }
                }
            },

            zip: {
                dist: {
                    cwd: 'dist/app/',
                    src: ['dist/app/dist/*', 'dist/app/images/*', 'dist/app/*'],
                    dest: 'dist/' + config.environment + '/<%= cfg.packageName %>.zip'
                }
            },

            unzip: {
                dist: {
                    src: 'dist/' + config.environment + '/<%= cfg.packageName %>.zip',
                    dest: 'dist/package/app/'
                }
            },

            war: {
                dist: {
                    options: {
                        war_dist_folder: 'dist/' + config.environment,
                        war_verbose: true,
                        war_name: '<%= cfg.packageName %>',
                        webxml_welcome: 'index.html',
                        webxml_display_name: 'OpenTele'
                    },
                    files: [
                        {
                            expand: true,
                            cwd: 'dist/app/',
                            src: ['*', 'dist/*', 'images/*'],
                            dest: ''
                        }
                    ]
                }
            },

            copy: {
                dist: {
                    files: [
                        {
                            expand: true,
                            dest: 'dist/',
                            src: [
                                'app/dist/*',
                                'app/images/*',
                                'app/index.html',
                                'app/style.css'
                            ]
                        },
                        {
                            flatten: true,
                            src: 'app/bower_components/pdfjs-dist/build/pdf.worker.js',
                            dest: 'dist/app/dist/libs.worker.js'
                        },
                        {
                            flatten: true,
                            src: 'app/config.js',
                            dest: 'dist/app/dist/config.js'
                        }
                    ]
                },
                dev: {
                    files: [
                        {
                            flatten: true,
                            src: 'app/bower_components/pdfjs-dist/build/pdf.worker.js',
                            dest: 'app/dist/libs.worker.js'
                        },
                        {
                            flatten: true,
                            src: 'app/config.js',
                            dest: 'app/dist/config.js'
                        }
                    ]
                }
            },

            protractor: {
                options: {
                    configFile: "e2e-tests/protractor.conf.js",
                    keepAlive: true,
                    noColor: true,
                    args: {
                    }
                },
                phantomjs: {
                    options: {
                        args: {
                            specs: protractorSpecs
                        },
                        configFile: "e2e-tests/protractor_phantomjs.conf.js"
                    }
                },
                chrome: {
                    options: {
                        args : {
                            capabilities: {
                                'browserName': 'chrome'
                            },
                            specs: protractorSpecs
                        }
                    }
                },
                firefox: {
                    options: {
                        args: {
                            capabilities: {
                                'browserName': 'firefox'
                            },
                            specs: protractorSpecs
                        }
                    }
                }
            },

            express: {
                options: {
                    // Override defaults here
                },
                fakeRestServer: {
                    options: {
                        port: 5000,
                        script: 'e2e-tests/mockRestApiServer/mockServer.js'
                    }
                }
            },

            ngconstant: {
                options: {
                    name: 'opentele.config',
                    dest: 'app/config.js'
                },
                e2e: {
                    constants: {
                        appConfig: {
                            version: "<%= pkg.version %>",
                            loggingEnabled: false,
                            loggingUrl: "http://localhost:3000/collectionapi/reports",
                            idleTimeoutInSeconds: 4,
                            idleWarningCountdownInSeconds: 4
                        },
                        restConfig: {
                            baseUrl: 'http://localhost:5000/opentele-citizen-server/'
                        }
                    }
                },
                dev: {
                    constants: {
                        appConfig: {
                            version: "<%= pkg.version %>",
                            loggingEnabled: false,
                            loggingUrl: "http://localhost:3000/collectionapi/reports",
                            idleTimeoutInSeconds: 600,
                            idleWarningCountdownInSeconds: 0.1
                        },
                        restConfig: {
                            baseUrl: 'http://localhost:5000/opentele-citizen-server/'
                        }
                    }
                },
                dist: {
                    constants: {
                        appConfig: {
                            version: "<%= pkg.version %>",
                            loggingEnabled: false,
                            loggingUrl: "http://localhost:3000/collectionapi/reports",
                            idleTimeoutInSeconds: 600,
                            idleWarningCountdownInSeconds: 0.1
                        },
                        restConfig: {
                            baseUrl: config.serverUrl 
                        }
                    }
                }
            }
        });

        // Loading of tasks and registering tasks
        grunt.loadNpmTasks('grunt-bower-task');
        grunt.loadNpmTasks('grunt-complexity');
        grunt.loadNpmTasks('grunt-contrib-clean');
        grunt.loadNpmTasks('grunt-contrib-concat');
        grunt.loadNpmTasks('grunt-contrib-connect');
        grunt.loadNpmTasks('grunt-contrib-copy');
        grunt.loadNpmTasks('grunt-contrib-jshint');
        grunt.loadNpmTasks('grunt-contrib-less');
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.loadNpmTasks('grunt-express-server');
        grunt.loadNpmTasks('grunt-html2js');
        grunt.loadNpmTasks('grunt-karma');
        grunt.loadNpmTasks('grunt-lesslint');
        grunt.loadNpmTasks('grunt-ng-annotate');
        grunt.loadNpmTasks('grunt-ng-constant');
        grunt.loadNpmTasks('grunt-protractor-runner');
        grunt.loadNpmTasks('grunt-war');
        grunt.loadNpmTasks('grunt-zip');
        grunt.loadNpmTasks('grunt-file-creator');

        grunt.registerTask('analyse', ['bower', 'jshint', 'complexity', 'lesslint']);

        grunt.registerTask('dev', ['bower', 'file-creator:plugins', 'ngconstant:dev', 'copy:dev',
            'express:fakeRestServer', 'connect:app', 'watch:dev']);
        grunt.registerTask('dev-no-net', ['file-creator:plugins', 'ngconstant:dev', 'copy:dev',
            'express:fakeRestServer', 'connect:app', 'watch:dev']);
        grunt.registerTask('compile', ['bower', 'file-creator:plugins', 'ngconstant:dev', 'copy:dev',
            'connect:app', 'jshint', 'html2js:dist',
            'concat:dev', 'ngAnnotate:opentele', 'clean:temp']);
        grunt.registerTask('dev-no-test', ['bower', 'file-creator:plugins', 'ngconstant:dev', 'copy:dev',
            'express:fakeRestServer', 'connect:app', 'watch:devCompile']);
        grunt.registerTask('dev-real-server', ['bower', 'file-creator:plugins',
            'ngconstant:devRealRest', 'copy:dev', 'connect:app', 'watch:devCompile']);

        var unitTestConfig = ['bower', 'file-creator:plugins', 'jshint', 'less:dev', 'ngconstant:dev',
            'html2js:dist'];
        grunt.registerTask('ci-unittest',
            unitTestConfig.concat('karma:ciUnit'));
        grunt.registerTask('unittest', unitTestConfig.concat('karma:unit'));
        grunt.registerTask('unittest-debug', unitTestConfig.concat('karma:debug'));

        var e2eBaseConfig = ['bower', 'file-creator:plugins', 'ngconstant:e2e', 'copy:dev',
            'express:fakeRestServer', 'connect:app', 'less:dev', 'html2js:dist',
            'concat:dev', 'ngAnnotate:opentele', 'clean:temp'];
        grunt.registerTask('e2etest-chrome',
            Array.prototype.concat(e2eBaseConfig, 'protractor:chrome'));
        grunt.registerTask('e2etest-firefox',
            Array.prototype.concat(e2eBaseConfig, 'protractor:firefox'));
        grunt.registerTask('e2etest-phantomjs',
            Array.prototype.concat(e2eBaseConfig, 'protractor:phantomjs'));
        grunt.registerTask('e2etest-all',
            Array.prototype.concat(e2eBaseConfig, ['protractor:chrome', 'protractor:firefox', 'protractor:phantomjs']));

        grunt.registerTask('package', ['bower', 'file-creator:plugins', 'ngconstant:dist', 'jshint',
            'less:prod', 'html2js:dist', 'karma:unit', 'concat:prod',
            'ngAnnotate:opentele', 'uglify:dist', 'clean:temp', 'copy:dist',
            'zip:dist', 'war:dist']);

        grunt.registerTask('ci-package', ['bower', 'file-creator:plugins', 'ngconstant:dist', 'jshint',
            'less:prod', 'html2js:dist', 'concat:prod', 'ngAnnotate:opentele',
            'uglify:dist', 'clean:temp', 'copy:dist', 'zip:dist', 'war:dist']);

        var ciE2EConfig = ['clean:dist', 'bower', 'file-creator:plugins', 'ngconstant:e2e',
            'jshint', 'less:prod', 'html2js:dist', 'concat:prod',
            'ngAnnotate:opentele', 'uglify:dist', 'clean:temp', 'copy:dist',
            'zip:dist', 'unzip:dist', 'express:fakeRestServer',
            'connect:package'];
        grunt.registerTask('ci-e2etest-chrome', ciE2EConfig.concat('protractor:chrome'));
        grunt.registerTask('ci-e2etest-firefox', ciE2EConfig.concat('protractor:firefox'));
        grunt.registerTask('ci-e2etest-phantomjs', ciE2EConfig.concat('protractor:phantomjs'));

    };

}());
