(function() {
    'use strict';

    describe('opentele.medicineList.restApiServices', function () {
		var httpBackend, httpProvider, medicineListSummary;

		beforeEach(module('opentele.medicineList.restApiServices'));

		beforeEach(inject(function($http, $httpBackend, _medicineListSummary_) {
			httpProvider = $http;
			httpBackend = $httpBackend;
			medicineListSummary = _medicineListSummary_;
		}));

        describe('get medicine list summary', function () {
            it('should get medicine list summary for user', function () {
                var user = {
                    links: {
                        medicineListSummary: "http://localhost/patient/medicinelistsummary"
                    }
                };
                httpBackend.whenGET(user.links.medicineListSummary).respond({
                    "isNew": false,
                    "links": {
                        "medicineList": "http://localhost/patient/medicinelist"
                    }
                });

                var successCallbackInvoked = false;
                var response = {};
                medicineListSummary.get(user, function(data) {
                    successCallbackInvoked = true;
                    response = data;
                });

                httpBackend.flush();
                expect(successCallbackInvoked).toEqual(true);
                expect(response.isNew).toBeFalsy();
                expect(response.links.medicineList).toBe('http://localhost/patient/medicinelist');
            });

            it('should throw exception if user has no medicine list summary link', function () {
                expect(function() {medicineListSummary.get({}, function(){});}).toThrow();
                expect(function() {medicineListSummary.get({links: {}}, function(){});}).toThrow();
            });
        });

        describe('get authorization header value', function () {
            it('should get value for authorization header', function () {
                httpProvider.defaults.headers.common.Authorization = 'secret';

                var headerValue = medicineListSummary.authorizationHeaderValue();

                expect(headerValue).toBe('secret');
            });
        });
    });
}());
