(function() {
    'use strict';

    describe('PluginRegistry', function () {
		var pluginRegistry;

		beforeEach(module('opentele.stateServices'));

		beforeEach(inject(function(_pluginRegistry_) {
			pluginRegistry = _pluginRegistry_;
		}));

		it('should be able to register plugin definition', function () {
			var myPlugin = {
				menuItem: {
					name: "menu name",
					url: "route",
					enabled: function(user) {}
				}
			};
			pluginRegistry.registerPlugin(myPlugin);

			expect(pluginRegistry.plugins).toContain(myPlugin);
			expect(pluginRegistry.plugins.length).toBe(1);
		});
    });
}());
