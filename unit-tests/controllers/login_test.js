(function () {
    'use strict';

    describe('opentele.controllers.login module', function () {

        beforeEach(module('opentele.controllers.login'));

        describe('loginCtrl tests', function () {
            var scope, location, translate, appContext, authentication,
                headerEnabled, serverInfo, nativeService, reminders,
                videoService, runController;

            module('opentele.stateServices');

            // mock services
            beforeEach(module(function ($provide) {
                reminders = {
                    update: function (user, onSuccess) {
                        this.updateCalled = true;
                    },
                    updateCalled: false
                };
                $provide.value('reminders', reminders);

                videoService = {
                    cancelPendingConferencePolling: function() {}
                };
                $provide.value('videoService', videoService);

                headerEnabled = {
                    set: function (val) {}
                };
                $provide.value('headerEnabled', headerEnabled);

                authentication = {
                    setError: false,
                    errorStatus: 401,
                    errorReason: 'BAD_CREDENTIALS',
                    login: function (patientUrl, username, password, success, error) {
                        if (this.setError === false) {
                            success({
                                'showRealtimeCTG': true,
                                'links': {
                                    'videoPendingConference': "http://localhost:" +
                                        "5000/opentele-citizen-server/rest/" +
                                        "conference/patientHasPendingConference"
                                }
                            });
                        } else {
                            error(this.errorStatus, {code: this.errorReason});
                        }
                    },
                    logoutCalled: false,
                    logout: function (onSuccess) {
                        this.logoutCalled = true;
                        onSuccess();
                    }
                };
                $provide.value('authentication', authentication);

                nativeService = {
                    getPatientPrivacyPolicy: function(callback) {
                        callback({text: "a patient privacy policy"});
                    }
                };
                $provide.value('nativeService', nativeService);

                serverInfo = {
                    get: function (onSuccess, onError) {
                        onSuccess(
                            {
                                links: {
                                    self: 'some/url'
                                }
                            });
                    }
                };
                $provide.value('serverInfo', serverInfo);
            }));

            // instantiate controller
            beforeEach(inject(function ($rootScope, $translate, $location,
                                        $controller, _appContext_,
                                        _headerEnabled_, _authentication_,
                                        _reminders_, _nativeService_,
                                        _videoService_) {
                scope = $rootScope.$new();
                location = $location;
                translate = $translate;
                appContext = _appContext_;
                runController = function () {
                    $controller('LoginCtrl', {
                        '$scope': scope,
                        '$location': location,
                        '$translate': translate,
                        'appContext': appContext,
                        'reminders': _reminders_,
                        'authentication': _authentication_,
                        'headerEnabled': _headerEnabled_,
                        'nativeService': _nativeService_,
                        'videoService': _videoService_
                    });
                };
            }));

            it('should open and close patient privacy policy popup when clicked', function() {
                runController();
                expect(scope.model.patientPrivacyPolicy).toBe('a patient privacy policy');
                expect(scope.model.showPopup).toEqual(false);
                scope.showPatientPrivacyPolicy();
                expect(scope.model.showPopup).toEqual(true);
                scope.hidePatientPrivacyPolicy();
                expect(scope.model.showPopup).toEqual(false);
            });

            it('should have a new path and have called services', function () {
                runController();
                scope.model = {
                    'username': 'nancyann',
                    'password': 'abcd1234'
                };

                scope.submit();

                expect(reminders.updateCalled).toEqual(true);

                expect(location.path()).toEqual("/menu");
            });

            it('should clear password when initializing page', function () {
                scope.model = {
                    'username': 'nancyann',
                    'password': 'abcd1234'
                };

                runController();

                expect(scope.model.username).toBe('nancyann');
                expect(scope.model.password).toBe('');
            });

            it('should show error message on failed login', function () {
                authentication.setError = true;
                runController();
                scope.model = {
                    'username': 'nancyann',
                    'password': 'abcd1234'
                };
                scope.submit();

                expect(scope.model.errorMessage).toEqual('BAD_CREDENTIALS');
            });

            it('should show error message when account is locked', function () {
                authentication.setError = true;
                authentication.errorReason = 'ACCOUNT_LOCKED';

                runController();
                scope.model = {
                    'username': 'nancyann',
                    'password': 'abcd1234'
                };
                scope.submit();

                expect(scope.model.errorMessage).toEqual('ACCOUNT_LOCKED');
            });

            it('should show server unavailable error message on unknown login error', function () {
                authentication.setError = true;
                authentication.errorStatus = 500;
                authentication.errorReason = 'UNKNOWN';

                runController();
                scope.model = {
                    'username': 'nancyann',
                    'password': 'abcd1234'
                };
                scope.submit();

                expect(scope.model.errorMessage).toEqual('OPENTELE_DOWN_TEXT');
            });

            it('should show error message instead of login form when server is unreachable', function () {
                serverInfo.get = function (success, error) {
                    error();
                };

                runController();

                expect(scope.model.showLoginForm).toBe(false);
                expect(scope.model.errorMessage).toMatch('OPENTELE_UNAVAILABLE_TEXT');
            });

            it('should show logged out error message when redirected to from other location', function () {
                appContext.requestParams.set('authenticationError', 'LOGGED_OUT');

                runController();

                expect(scope.model.errorMessage).toEqual('LOGGED_OUT');
            });

            it('should log current user out when navigating to login page', function () {
                appContext.currentUser.set({
                    firstName: 'foo',
                    lastName: 'bar'
                });

                runController();

                expect(authentication.logoutCalled).toBe(true);
                expect(appContext.currentUser.get()).toEqual({});
            });

            it('should cancel pending conference polling when logging out', function() {
                appContext.currentUser.set({
                    firstName: 'foo',
                    lastName: 'bar'
                });
                spyOn(videoService, 'cancelPendingConferencePolling');

                runController(); // calls log out

                expect(videoService.cancelPendingConferencePolling).toHaveBeenCalled();
            });
        });
    });
}());
