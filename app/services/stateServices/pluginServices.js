(function() {
    'use strict';

    var pluginServices = angular.module('opentele.stateServices.pluginServices', []);

	pluginServices.service('pluginRegistry', function() {
		var plugins = [];
		var registerPlugin = function(pluginDefinition) {
			plugins.push(pluginDefinition);
		};

		return {
			registerPlugin: registerPlugin,
			plugins: plugins
		};
	});
}());
