(function() {
    'use strict';

    angular.module('opentele.medicineList.restApiServices', [])
	.service('medicineListSummary', function($http) {
		return {
			get: function(user, onSuccess) {

				if (!user.hasOwnProperty('links') || !user.links.hasOwnProperty('medicineListSummary')) {
					throw new TypeError('User object does not contain a link relation to medicineListSummary');
				}

				var url = user.links.medicineListSummary;
				$http.get(url)
					.success(onSuccess);
			},
            authorizationHeaderValue: function() {
                return $http.defaults.headers.common.Authorization;
            }
		};
	});
}());
