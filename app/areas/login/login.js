(function () {
    'use strict';

    var login = angular.module('opentele.controllers.login', [
        'ngRoute',
        'opentele.stateServices',
        'opentele.restApiServices',
        'opentele.translations',
        'opentele-commons.nativeServices'
    ]);

    login.config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                title: 'OPENTELE',
                templateUrl: 'areas/login/login.html',
                publicAccess: true
            })
            .when('/login', {
                title: 'OPENTELE',
                templateUrl: 'areas/login/login.html',
                publicAccess: true
            });
    });

    login.controller('LoginCtrl', function ($scope, $location, appContext,
                                            serverInfo, authentication,
                                            reminders, headerService,
                                            nativeService, videoService) {

        var setupErrorMessage = function() {
            var key = 'authenticationError';
            if (!appContext.requestParams.containsKey(key)) {
                $scope.model.errorMessage = "";
                return;
            }

            var reason = appContext.requestParams.getAndClear(key);
            $scope.model.errorMessage = reason;
        };

        var setupLoginForm = function(root) {
            $scope.model.showLoginForm = true;
            setupErrorMessage();

            var onSuccessfulLogin = function(user) {
                appContext.currentUser.set(user);
                reminders.update(appContext.currentUser.get());

                if (user.passwordExpired === true) {
                    appContext.requestParams.set('forceChange', true);
                    $location.path('/change_password');
                    return;
                }
                $location.path('/menu');
            };

            var onError = function(status, response) {
                switch(status) {
                    case 401:
                        $scope.model.errorMessage = response.code;
                        break;
                    default:
                        $scope.model.errorMessage = 'OPENTELE_DOWN_TEXT';
                }
            };

            $scope.showPatientPrivacyPolicy = function() {
                $scope.model.showPopup = true;
            };

            $scope.hidePatientPrivacyPolicy = function() {
                $scope.model.showPopup = false;
            };

            $scope.submit = function() {
                authentication.login(root.links.patient, $scope.model.username,
                    $scope.model.password, onSuccessfulLogin, onError);
            };

            nativeService.getPatientPrivacyPolicy(function(patientPrivacyPolicyResponse) {
                $scope.model.patientPrivacyPolicy = patientPrivacyPolicyResponse.text;
            });
        };

        var connectionError = function() {
            $scope.model.errorMessage = 'OPENTELE_UNAVAILABLE_TEXT';
        };

        var setInitialState = function() {
            headerService.setHeader(false);
            if (typeof ($scope.model) === 'undefined') {
                $scope.model = {};
            }
            $scope.model.showPopup = false;
            $scope.model.showLoginForm = false;
            $scope.model.password = '';
            $scope.model.errorMessage = '';
            $scope.model.patientPrivacyPolicy = '';

            logout();
        };

        var logout = function() {
            videoService.cancelPendingConferencePolling();
            authentication.logout(function () {
                appContext.currentUser.clear();
            });
        };

        setInitialState();
        serverInfo.get(setupLoginForm, connectionError);
    });
}());
