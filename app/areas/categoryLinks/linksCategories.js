(function() {
    'use strict';

    var linksCategories = angular.module('opentele.controllers.linksCategories', [
        'ngRoute',
        'opentele.stateServices',
        'opentele.restApiServices'
    ]);

    linksCategories.config(function ($routeProvider) {
            $routeProvider.when('/links_categories', {
                title: 'LINKS_TITLE',
                templateUrl: 'areas/categoryLinks/linksCategories.html'
            });
        });

    linksCategories.controller('LinksCategoriesCtrl', function ($scope, $location, headerService,
                                                                appContext, linksCategories) {

        $scope.showLinksCategory = function(index) {
            var linksCategory = $scope.model.categories[index];
            appContext.requestParams.set('selectedLinksCategory', linksCategory);
            $location.path('/category_links');
        };

        headerService.setBack(false);
        $scope.model = {};
        var user = appContext.currentUser.get();

        linksCategories.listFor(user, function(data) {
            $scope.model = data;
            if ($scope.model.categories.length === 1) {
                $location.replace('/links_categories', '/menu');
                var firstCategory = 0;
                $scope.showLinksCategory(firstCategory);
            }
        });

    });

}());
