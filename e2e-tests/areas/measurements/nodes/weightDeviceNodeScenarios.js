(function() {
    'use strict';

    describe('walk through and fill out questionnaire containing weight devide node', function() {
        var loginPage = require('../../login/loginPage.js');
        var menuPage = require('../../menu/menuPage.js');
        var measurementsPage = require('../performMeasurementsPage.js');
        var questionnairePage = require('../questionnairePage.js');
        var ioNodePage = require('./ioNodePage.js');
        var weightDeviceNodePage = require('./weightDeviceNodePage.js');

        beforeEach(function () {
            loginPage.get();
            loginPage.doLogin('nancyann', 'abcd1234');
            menuPage.toMeasurements();
            measurementsPage.toQuestionnaire("Vejning", "0.1");
        });

        it('should show questionnaire', function () {
            ioNodePage.headings.then(function(items) {
                expect(items.length).toBe(1);
                expect(items[0].getText()).toMatch(/Find vægten frem/);
            });
        });

        it('should navigate to weight device node', function () {
            questionnairePage.clickCenterButton();
            expect(weightDeviceNodePage.heading.getText()).toMatch(/Weight/);
            expect(weightDeviceNodePage.info.getText()).toMatch(/Tænd for vægten/);
        });

        it('should see all info texts and fill out form', function(done) {
            questionnairePage.clickCenterButton();
            expect(weightDeviceNodePage.heading.getText()).toMatch(/Weight/);
            expect(weightDeviceNodePage.info.getText()).toMatch(/Tænd for vægten/);

            var infoDeferred = protractor.promise.defer();
            var weightDeferred = protractor.promise.defer();
            setTimeout(function() {
                weightDeviceNodePage.info.getText().then(
                    function(text) {
                        infoDeferred.fulfill(text);
                    });
                weightDeviceNodePage.weight.getAttribute('value').then(
                    function(value) {
                        weightDeferred.fulfill(value);
                        done();
                    });
            }, 3500);
            expect(weightDeferred).toEqual('85.3');
            expect(infoDeferred).toMatch(/Connected/);

        });
    });

}());
