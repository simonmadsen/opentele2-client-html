(function() {
    'use strict';

    describe('walk through and fill out questionnaire containing temperature devide node', function() {
        var loginPage = require('../../login/loginPage.js');
        var menuPage = require('../../menu/menuPage.js');
        var measurementsPage = require('../performMeasurementsPage.js');
        var questionnairePage = require('../questionnairePage.js');
        var ioNodePage = require('./ioNodePage.js');
        var temperatureDeviceNodePage = require('./temperatureDeviceNodePage.js');

        beforeEach(function () {
            loginPage.get();
            loginPage.doLogin('nancyann', 'abcd1234');
            menuPage.toMeasurements();
        });

        it('should navigate to temperature device node', function () {
            measurementsPage.toQuestionnaire("Temperatur", "0.1");
            
            expect(temperatureDeviceNodePage.heading.getText()).toMatch(/Temperature/);
        });

        it('should see all info texts and fill out form', function(done) {
            measurementsPage.toQuestionnaire("Temperatur", "0.1");
            
            expect(temperatureDeviceNodePage.heading.getText()).toMatch(/Temperature/);

            var infoDeferred = protractor.promise.defer();
            var temperatureDeferred = protractor.promise.defer();
            setTimeout(function() {
                temperatureDeviceNodePage.info.getText().then(
                    function(text) {
                        infoDeferred.fulfill(text);
                    });
                temperatureDeviceNodePage.temperature.getAttribute('value').then(
                    function(value) {
                        temperatureDeferred.fulfill(value);
                        done();
                    });
            }, 4500);

            expect(temperatureDeferred).toEqual('37.3');
            expect(infoDeferred).toMatch(/Connected/);
        });
    });

}());
