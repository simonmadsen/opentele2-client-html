(function() {
    'use strict';

    describe('linksCategories', function() {
        var loginPage = require("../login/loginPage.js");
        var menuPage = require("../menu/menuPage.js");
        var linksCategoriesPage = require("./linksCategoriesPage.js");
        var categoryLinksPage = require("./categoryLinksPage.js");

        describe('when authenticated', function() {

            beforeEach(function() {
                loginPage.get();
                loginPage.doLogin('nancyann', 'abcd1234');
                menuPage.toLinksCategories();
                var informationCategoryIndex = 0;
                linksCategoriesPage.toLinksCategory(informationCategoryIndex);
            });

            it('should navigate to information category links page', function() {
                expect(browser.getLocationAbsUrl()).toMatch("/category_links");
                expect(categoryLinksPage.title.getText()).toMatch('Information');
                expect(categoryLinksPage.categoryLinksList).toBeDefined();
            });

            it('should show list of link names', function() {
                categoryLinksPage.categoryLinksNames.then(function(names) {
                    expect(names.length).toEqual(2);
                    expect(names[0].getText()).toMatch(/OpenTele user manual/i);
                    expect(names[1].getText()).toMatch(/Saturation measurement guide/i);
                });
            });

            it ('should show popup when clicking a link', function() {
                var saturationMeasurementGuideIndex = 1;
                categoryLinksPage.showPopup(saturationMeasurementGuideIndex);
                expect(categoryLinksPage.lightBoxDimmer.isDisplayed()).toBe(true);
            });

            it ('should show and then close popup when clicking the back button in the popup', function() {
                var saturationMeasurementGuideIndex = 1;
                categoryLinksPage.showPopup(saturationMeasurementGuideIndex);
                expect(categoryLinksPage.lightBoxDimmer.isDisplayed()).toBe(true);
                categoryLinksPage.hidePopup();
                expect(categoryLinksPage.lightBoxDimmer.isDisplayed()).toBe(false);
            });

            it ('should show and then follow link when clicking the continue button in the popup', function() {
                var saturationMeasurementGuideIndex = 1;
                categoryLinksPage.showPopup(saturationMeasurementGuideIndex);
                expect(categoryLinksPage.lightBoxDimmer.isDisplayed()).toBe(true);
                browser.getCurrentUrl().then(function(lastUrl) {
                    var oldUrl = lastUrl;
                    element(by.id('light-box-continue-button')).click().then(function() {
                        browser.driver.wait(function() { // wait for fakeNativeLayer to navigate to the requested URL... (see http://docsplendid.com/tags/wait-for-element)
                            var deferred = protractor.promise.defer();
                            browser.driver.getCurrentUrl()
                                .then(function (url) {
                                    deferred.fulfill(url !== oldUrl);
                                });
                            return deferred.promise;
                        });
                        
                        expect(browser.driver.getCurrentUrl()).toMatch(/opentele.silverbullet.dk\/architecture/i);
                    });
                });
            });
        });
    });
}());