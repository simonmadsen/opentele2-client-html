(function() {
    'use strict';

    describe('linksCategories', function() {
        var loginPage = require("../login/loginPage.js");
        var menuPage = require("../menu/menuPage.js");
        var linksCategoriesPage = require("./linksCategoriesPage.js");

        describe('when authenticated', function() {

            beforeEach(function() {
                loginPage.get();
                loginPage.doLogin('nancyann', 'abcd1234');
                menuPage.toLinksCategories();
            });

            it('should navigate to links categories page', function() {
                expect(browser.getLocationAbsUrl()).toMatch("/links_categories");
                expect(linksCategoriesPage.title.getText()).toMatch('Information and guidance');
                expect(linksCategoriesPage.linksCategoriesList).toBeDefined();
            });

            it('should show list of links categories names', function() {
                linksCategoriesPage.linksCategoriesNames.then(function(names) {
                    expect(names.length).toEqual(2);
                    expect(names[0].getText()).toMatch(/Information/i);
                    expect(names[1].getText()).toMatch(/Video instructions/i);
                });
            });

            it ('should redirect to links category page when link clicked', function() {
                linksCategoriesPage.toLinksCategory(0);
                expect(browser.getLocationAbsUrl()).toMatch(/\/category_links/);
            });

        });

        describe('patient with only one link category available', function() {
            beforeEach(function() {
                loginPage.get();
                loginPage.doLogin('rene', '1234');
            });

            it('should automatically redirect to link category', function () {
                menuPage.toLinksCategories();
                expect(browser.getLocationAbsUrl()).toMatch(/\/category_links/);
            });
        });

    });
}());
