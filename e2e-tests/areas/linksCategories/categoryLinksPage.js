(function() {
    'use strict';

    var CategoryLinksPage = function () {
        this.backButton = element(by.id('back-button'));
        this.menuButton = element(by.id('menu-button'));
        this.title = element(by.id('category-title'));
        this.categoryLinksList = element(by.id('category-links-list'));
        this.categoryLinksNames = element.all(by.binding('linkRef'));
        this.lightBoxDimmer = element(by.id('light-box'));
        this.lightBox = element(by.id('light-box'));

        this.get = function () {
            browser.get('index.html#/category_links');
        };

        this.showPopup = function (index) {
            element.all(by.id('category-links-items')).
                get(index).
                $('a').
                click();
        };

        this.hidePopup = function () {
            element(by.id('light-box-back-button')).click();
        };

    };

    module.exports = new CategoryLinksPage();

}());
