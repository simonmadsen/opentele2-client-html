(function() {
    'use strict';

    exports.get = {
        "name": "Vejning",
        "id": 38,
        "startNode": "278",
        "endNode": "280",
        "nodes": [
            {
                "WeightDeviceNode": {
                    "helpImage": null,
                    "helpText": null,
                    "deviceId": {
                        "type": "String",
                        "name": "279.WEIGHT#DEVICE_ID"
                    },
                    "weight": {
                        "type": "Float",
                        "name": "279.WEIGHT"
                    },
                    "text": "Tænd for vægten og afvent ny besked i skærmbillede",
                    "nextFail": "AN_279_CANCEL",
                    "next": "ANSEV_280_D279",
                    "nodeName": "279"
                }
            },
            {
                "AssignmentNode": {
                    "expression": {
                        "value": true,
                        "type": "Boolean"
                    },
                    "variable": {
                        "type": "Boolean",
                        "name": "279.WEIGHT#CANCEL"
                    },
                    "next": "ANSEV_280_F279",
                    "nodeName": "AN_279_CANCEL"
                }
            },
            {
                "AssignmentNode": {
                    "expression": {
                        "value": "GREEN",
                        "type": "String"
                    },
                    "variable": {
                        "type": "String",
                        "name": "279.WEIGHT#SEVERITY"
                    },
                    "next": "280",
                    "nodeName": "ANSEV_280_F279"
                }
            },
            {
                "AssignmentNode": {
                    "expression": {
                        "value": "GREEN",
                        "type": "String"
                    },
                    "variable": {
                        "type": "String",
                        "name": "279.WEIGHT#SEVERITY"
                    },
                    "next": "280",
                    "nodeName": "ANSEV_280_D279"
                }
            },
            {
                "IONode": {
                    "elements": [
                        {
                            "TextViewElement": {
                                "text": "Find v\u00e6gten frem, og tryk n\u00e6ste n\u00e5r du er klar til at blive vejet"
                            }
                        },
                        {
                            "ButtonElement": {
                                "next": "279",
                                "gravity": "center",
                                "text": "N\u00e6ste"
                            }
                        }
                    ],
                    "nodeName": "278"
                }
            },
            {
                "EndNode": {
                    "nodeName": "280"
                }
            }
        ],
        "output": [
            {
                "type": "String",
                "name": "279.WEIGHT#DEVICE_ID"
            },
            {
                "type": "Float",
                "name": "279.WEIGHT"
            },
            {
                "type": "Boolean",
                "name": "279.WEIGHT#CANCEL"
            },
            {
                "type": "String",
                "name": "279.WEIGHT#SEVERITY"
            }
        ]
    };
}());
