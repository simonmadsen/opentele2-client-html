(function() {
    'use strict';

    var utils = require("./utils.js");

    exports.pendingConference = function(req, res) {

        var userToken = utils.decodeAuthorizationHeader(req);
        console.log("Pending conference requested from: " + userToken);

        var pendingConference = {
            roomKey: "42",
            serviceUrl: "http://www.example.com/someServiceUrl"
        };
        
        setTimeout(function() {
            console.log("Pending conference returned.");
            res.send(pendingConference);
        }, 5000);
    };

    exports.pendingMeasurement = function(req, res) {
        console.log("pending measurements called");

        var pendingMeasurement = {
            type: "SATURATION"
        };

        res.send(pendingMeasurement);
    };

    var retryCount = 0;
    exports.measurementFromPatient = function(req, res) {
        console.log("Measurement from patient called");

        var userToken = utils.decodeAuthorizationHeader(req);
        if (userToken.indexOf("uploadfails") > -1 && retryCount < 2) {
            retryCount += 1;
            res.status(500).end();
            return;
        }

        retryCount = 0;
        res.status(200).end();
    };

}());
