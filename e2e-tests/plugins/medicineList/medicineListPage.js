(function() {
    'use strict';

    var MedicineListPage = function () {
        this.menuButton = element(by.id('menu-button'));
        this.title = element(by.css('.title'));
        this.pdfViewer = element(by.id('medicine-list-view'));
        this.prevPageButton = element(by.id('pdf-prev-button'));
        this.nextPageButton = element(by.id('pdf-next-button'));
        this.zoomInButton = element(by.id('pdf-zoom-in-button'));
        this.zoomOutButton = element(by.id('pdf-zoom-out-button'));

        this.get = function () {
            browser.get('index.html#/medicine_list');
        };

    };

    module.exports = new MedicineListPage();

}());
