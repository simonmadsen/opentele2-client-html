(function() {
    'use strict';

    describe('medicine list', function () {
		var loginPage = require("../../areas/login/loginPage.js");
		var menuPage = require("../../areas/menu/menuPage.js");
		var medicineListPage = require("./medicineListPage.js");

		describe('user with medicine list', function() {
			beforeEach(function() {
				loginPage.get();
				loginPage.doLogin('nancyann', 'abcd1234');
				menuPage.toMedicineList();
			});

			it('should show pdf navigation buttons', function() {
				expect(medicineListPage.prevPageButton).toBeDefined();
				expect(medicineListPage.nextPageButton).toBeDefined();
				expect(medicineListPage.zoomInButton).toBeDefined();
				expect(medicineListPage.zoomOutButton).toBeDefined();
			});

			it('should show pdf viewer when document loaded', function() {
				browser.sleep(500); // wait for pdf to be loaded async from server
				expect(medicineListPage.pdfViewer).toBeDefined();
			});
		});

		describe('user with no medicine list', function () {
			beforeEach(function() {
				loginPage.get();
				loginPage.doLogin('rene', '1234');
			});

			it('should not have a menu item for medicine list', function () {
				menuPage.menuItems.then(function(items) {
					items.forEach(function(item) {
						expect(item.getText()).not.toContain('Medicine');
					});
				});
			});
		});

    });
}());
